#!/usr/bin/env bash

# This script will generate all programs to test Invalid Instruction traps. We
# define an Invalid Instruction as a type of exception which can only occur in
# the decoder, when the instruction provided has a valid OP code but fails the
# remaining elements of the spec.
sed '34s/00/40/' hex/slli.hex > hex/slli_trap.hex
sed '34s/00/80/' hex/srli.hex > hex/srli_trap.hex
sed '34s/40/80/' hex/srai.hex > hex/srai_trap.hex
sed '35s/00/80/' hex/add.hex > hex/add_trap.hex
sed '35s/40/80/' hex/sub.hex > hex/sub_trap.hex
sed '35s/00/80/' hex/sll.hex > hex/sll_trap.hex
sed '35s/00/80/' hex/slt.hex > hex/slt_trap.hex
sed '35s/00/80/' hex/sltu.hex > hex/sltu_trap.hex
sed '35s/00/80/' hex/xor.hex > hex/xor_trap.hex
sed '35s/00/80/' hex/srl.hex > hex/srl_trap.hex
sed '35s/40/80/' hex/sra.hex > hex/sra_trap.hex
sed '35s/00/80/' hex/or.hex > hex/or_trap.hex
sed '35s/00/80/' hex/and.hex > hex/and_trap.hex
sed '33s/8/b/' hex/beq.hex > hex/beq_trap.hex
sed '33s/1/2/' hex/bne.hex > hex/bne_trap.hex
sed '33s/4/3/' hex/blt.hex > hex/blt_trap.hex
sed '33s/55/52/' hex/bge.hex > hex/bge_trap.hex
sed '33s/e/b/' hex/bltu.hex > hex/bltu_trap.hex
sed '33s/7/3/' hex/bgeu.hex > hex/bgeu_trap.hex
sed '34s/06/66/' hex/sb_lb.hex > lb_trap.hex
sed '33s/0e/36/' hex/sb_lb.hex > sb_trap.hex
sed '38s/16/36/' hex/sb_lh.hex > lh_trap.hex
sed '35s/26/76/' hex/sw_lw.hex > lw_trap.hex
sed '34s/2c/6c/' hex/sw_lw.hex > sw_trap.hex
sed '34s/46/36/' hex/sb_lbu.hex > lbu_trap.hex
sed '38s/56/66/' hex/sh_lhu.hex > lhu_trap.hex
sed '36s/1e/5e/' hex/sh_lh.hex > sh_trap.hex
