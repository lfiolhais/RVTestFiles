#include "init.S"

  li a0, 1
  li a1, 1

  bne  a0, a1, exit    // a0=a1 so it will not branch
  addi a1, a1, 1       // this instruction will be executed
  bne  a0, a1, exit    // a0=a1 so it will jump to exit
  addi t1, t1, 1       // t1=0 because it will not be executed

exit:
#include "exit.S"
