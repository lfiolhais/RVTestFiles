#include "init.S"

  li a0, -10
  li a1, 20

  bge  a0, a1, exit               // a0<a1 so it will not branch
  addi a0, a0, 30                 // this instruction will be executed
  bge  a0, a1, branch_if_equal    // a0=a1 so it will jump to branch_if_equal
  addi t1, t1, 1                  // t1=0 because it will not be executed

branch_if_equal:
  addi a0, a0, 30      // this instruction will be executed
  bge  a0, a1, exit    // a0>a1 so it will branch to exit
  addi t2, t2, 1       // t2=0 because it will not be executed

exit:
#include "exit.S"
